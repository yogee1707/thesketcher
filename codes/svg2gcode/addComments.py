file_name = "aman.gcode"  # put your filename here

with open(file_name, 'r+') as f:

    new_code = "/home/pi/gcodes"            # where the new modified code will be put
    content = f.readlines()  # gcode as a list where each element is a line 

    for line in content: 
        gcode = line.strip('\n').split(";")  # seperates the gcode from the comments 
        if 'M280 PO S70 ':  
            gcode = 'SERVO UP'
        new_code += gcode + ';'  # rebuild the code from the modified pieces

    f.seek(0)           # set the cursor to the beginning of the file
    f.write(new_code)   # write the new code over the old one