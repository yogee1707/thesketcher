# theSketcher's README

## Making gcodes

### 1 using the svg2gcode at github
- complex images are not able to get converted into the gcodes
- images made in the inkscape are not compatible
	- .eps
	- .svg optimized
- svg2gcode working with plain svg format, the issue is while defining the width with the string 'mm'

> erros displayed : Input File: tests/yStar.svg
Output File: tests/gcode_output/yStar.gcode
Log File: tests/log/yStar.log

Traceback (most recent call last):
  File "svg2gcode.py", line 206, in <module>
    generate_gcode(file)
  File "svg2gcode.py", line 78, in generate_gcode
    scale_x = bed_max_x / float(width)
ValueError: invalid literal for float(): 50mm


* need to test on linux => http://pycam.sourceforge.net/ *

### 2 Generating gcodes using inkscape
- Inkscape is generating edge images of complex images well
- Gcodes are generated and are working well
- G29 is set outside the plane (co ordinates)